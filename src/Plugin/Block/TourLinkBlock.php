<?php

namespace Drupal\tour_link_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Tour link' block.
 *
 * @Block(
 *   id = "tour_link",
 *   admin_label = @Translation("Tour link block"),
 *   category = @Translation("Tour"),
 * )
 */
class TourLinkBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new TourLinkBlock instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager, RouteMatchInterface $route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityManager = $entity_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    if (!\Drupal::currentUser()->hasPermission('access tour')) {
      return AccessResult::forbidden();
    }

    $route_name = $this->routeMatch->getRouteName();

    $results = \Drupal::entityQuery('tour')
      ->condition('routes.*.route_name', $route_name)
      ->execute();

    $controller = $this->entityManager->getStorage('tour');

    if (!empty($results) && $tours = $controller->loadMultiple(array_keys($results))) {
      foreach ($tours as $id => $tour) {
        // Match on params.
        if (!$tour->hasMatchingRoute($route_name, $this->routeMatch->getRawParameters()->all())) {
          unset($tours[$id]);
        }
      }
      if (!empty($tours)) {
        return AccessResult::allowed();
      }
    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'tour_link_block_link',
      '#tour_link' => '?tour=1',
    ];
  }

}
